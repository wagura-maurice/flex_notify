<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Qwetu;
use App\Helpers\Notify;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function($app) {
	// Routes for sms notifications
	$app->group(['prefix' => 'sms'], function($app) {
		// instantly send sms notification without having it queue for later processing.
		$app->post('papohapo', function (Request $request) {

			$this->validate($request, Qwetu::$rules);

			$request['qwetu_status'] = 'sent';

			Qwetu::create($request->all());

			return Notify::papohapoSMS($request->to, $request->message);
		});
		// add sms notification to db for later processing. (by queue)
		$app->post('queue', function (Request $request) {

			$to = [];
			foreach (explode(',', $request->to) as $phone) {
				$to[] = Notify::suffix($phone);
			}

			$request['to'] = implode(',', $to);

			$this->validate($request, Qwetu::$rules);

			return Notify::respond(Response::HTTP_CREATED, Qwetu::create($request->all()));

		});
		// proccess sms notifications that have been queued. (set a per min cron job worker)
		$app->get('worker', function () {

			return Notify::respond(Response::HTTP_OK, Notify::SMS_WORKER());
		});
	});
});

