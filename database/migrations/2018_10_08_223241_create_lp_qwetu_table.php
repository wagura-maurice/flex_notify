<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpQwetuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_qwetu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('to');
            $table->string('message');
            $table->enum('qwetu_status', [
                'processing',
                'sent',
                'not-sent'
            ])->default('processing');
            $table->longtext('last_api_report')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_qwetu');
    }
}
