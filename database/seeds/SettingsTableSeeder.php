
<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Setting::unguard();

        DB::table('lp_setting')->delete();

        $Settings = array(
            [
                'name' => 'AT_username',
                'value' => 'sandbox',
                'setting_status' => 'active'
            ],
            [
                'name' => 'AT_apiKey',
                'value' => 'a030f97ce7afaf325f7f48825865b706d87883458313242476977d5cd6690761',
                'setting_status' => 'active'
            ],
            [
                'name' => 'country_code',
                'value' => '254',
                'setting_status' => 'active'
            ]
        );
        // Loop through each setting above and create the record for them in the database
        foreach ($Settings as $Setting) {
            Setting::create($Setting);
        }
        
        Setting::reguard();
    }
}
