<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qwetu extends Model {

    protected $table = 'lp_qwetu';

    protected $fillable = [
    	'to',
        'message',
        'qwetu_status',
        'last_api_report'
    ];

    public static $rules = [
        'to' => 'required|string',
        'message' => 'required|string'
    ];

}
