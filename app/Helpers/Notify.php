<?php

namespace App\Helpers;

use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Http\Response;

class Notify {

	public static function papohapoSMS($to, $message) {

		$AT = new AfricasTalking(Self::getSetting("AT_username"), Self::getSetting("AT_apiKey"));
        $SMS = $AT->sms();
        $response = $SMS->send([
            'to' => [$to],
            'message' => trim($message)
        ]);

        return Self::respond(Response::HTTP_OK, $response);
	}

	public static function SMS_WORKER() {

        $AT = new AfricasTalking(Self::getSetting("AT_username"), Self::getSetting("AT_apiKey"));
        $SMS = $AT->sms();

        $return = [];
        $qwetus = \App\Qwetu::where(['qwetu_status' => 'processing'])->get();
        foreach ($qwetus as $qwetu) {
            $response = $SMS->send([
                'to' => [$qwetu->to],
                'message' => trim($qwetu->message)
            ]);

            if ($response['status'] == 'success') {
                $update = \App\Qwetu::find($qwetu->id);
                $update->update([
                    'qwetu_status' => 'sent',
                    'last_api_report' => json_encode($response)
                ]);
            }
            
            $return[] = $response;
        }

        return Self::respond(Response::HTTP_OK, json_decode(json_encode($return)));
 
    }

    public static function respond($status, $data = []) {
        return response()->json($data, $status);
    }

	public static function getSetting($value) {
        $setting = \App\Setting::where('name', $value)->first();
        return (isset($setting->value) ? $setting->value : 'not found');

    }

	public static function column($model, $column, $value, $return) {
		$Model = '\\App\\'.$model;
		return $Model::where([$column => $value])->first()->$return;
	}

	public static function appURL() {
		return !is_null(env('APP_URL')) ? env('APP_URL') : 'http://127.0.0.1:8000';
	}

    public static function suffix($phone) {
        return Self::getSetting("country_code") . substr($phone, -9);
    }
}
