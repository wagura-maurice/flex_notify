<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

    protected $table = 'lp_setting';

    protected $fillable = [
    	'name',
    	'value',
    	'setting_status'
    ];

    public static $rules = [
        'name' => 'required|unique:lp_setting',
    ];

}
